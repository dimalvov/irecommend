imputeNas <- function(df, magic=0){
  df %>% replace(.,is.na(.),magic)
}

normalizeData <- function(df) {
  #rework needed probably
  df %>% replace(.,.>0,1)
}

makeValidata <- function(mat){
  tests <- sample(length(mat),size = )
}

maskInteraction <- function(vect, mask=NA, except='aux_') {
  masked <- which(vect>0 & !grepl(except, colnames(vect)))
  if(length(masked)==0) {
    stop("No observations > 0")
  }
  vect[sample(masked,1)] <- mask
  vect
}

assessGuess <- function(truth, model, n=10, method='g', f=NULL, debug=F,...){
  
  #multiple IO models
  if(length(truth)>1 & is.list(truth)){
    truth_aux <- truth[[2:length(truth)]]
    truth <- truth[[1]]
  } else {
    truth_aux <- NULL
  }
  
  masked <- truth %>% maskInteraction()
  masked.truth <- truth[,is.na(masked), drop=F]
  masked[is.na(masked)] <- 0
  
  if(length(masked.truth)>0){
    
    if(method=='g'){#predict with net model
    guess <- list(masked, truth_aux)  %>% netPredictOne(., model=model, n=n)
    
    guessed <- guess$output
    
    } else if(method=='f'){#get most frequent interactions
      guessed <- f
      
    } else if(method=='r'){#random guess
      guessed <- sample(truth,n)
      
    } else if(method=='ibcf'){
      masked <- as(masked,"binaryRatingMatrix")
      guessed <- predict(model, newdata=masked, n=n)
      guessed <- as(guessed,"list")[[1]]
      names(guessed) <- guessed
    }
    
    if(debug==T){
      res <- ifelse(dimnames(masked.truth)[[2]] %in% names(guessed),dimnames(masked.truth)[[2]],0)
    } else {
      res <- ifelse(dimnames(masked.truth)[[2]] %in% names(guessed),1,0)
    }
    
  } else {res<-NA}

  return(res)
}

netPredictOne <- function(vect, model, n=10, dict=NULL, cover=NULL, method='net'){
  #prediction:input list with product id, quantity output n top predictions
  #dict adds text descriptions, for testing
  #cover looks for most promising preds only in cover list
  
  require(keras)
  require(dplyr)
  
  #when multiple IO in the predictions, the sales vect is always first
  if(length(vect) > 1 & is.list(vect)) {
    vect_aux <- vect[[2:length(vect)]]
    vect <- vect[[1]]
  } else {
    vect_aux <- NULL
  }
  
  vect <- vect %>% imputeNas()
  
  if(!is.null(vect_aux)){
    vect <- list(vect, vect_aux)
  }

  if( method == 'net'){
    pred <- vect %>%
      predict(model,x=.) 
    
    if(length(pred)>1 & is.list(pred)){
      pred <- pred[[1]]
      prednames <- dimnames(vect[[1]])[[2]]
      names(pred) <- prednames[!grepl('aux_', prednames)]
      vect <- vect[[1]]
    } else {
      prednames <- dimnames(vect)[[2]]
      names(pred) <- prednames[!grepl('aux_', prednames)]
    }
    
  } else if(method == 'ibcf'){
    require(recommenderlab)
    vect.c <- vect %>% as(.,"binaryRatingMatrix")
    pred <- vect.c %>% 
      predict(model, newdata=., n = n)
    pred <- as(pred,"list")[[1]]
    names(pred) <- pred
    prednames <- dimnames(vect)[[2]]
  }

  ins <- vect[vect > 0]
  names(ins) <- prednames[which(vect>0)]
  pred <- pred[!(names(pred) %in% names(ins))]
  
  if(length(cover > 0)){
    pred <- pred[names(pred) %in% cover]
  }
  
  keeps <- sort(pred, decreasing = T, index.return =T)

  #return
  list(
    input=ins, 
    output=pred[keeps$ix[1:n]]
    )
}

textPredict <- function(prediction, dict){
  out <- lapply(prediction, function(x) {
    
    #names(x)
    nms <- names(x)
    if(!(length(nms)>0)){
     nms<-x
    }
    lapply(nms, function(y){ dict[as.character(y)]}) %>% unlist(use.names = T)
     })
  
  out
}

transformData <- function(df, min.items=2){
  require(data.table)

  df %<>% as.data.table() %>% 
    dcast.data.table(., user ~ prod, fun.aggregate = length, value.var = 'quan') %>% 
    as.data.frame() %>% 
    mutate_at(.,vars(-user), funs(normalizeData))
  
    rownames(df) <- df$user
  
  df %<>% 
    select(-user)
  
  #incd <- rowSums(df)
  #qq <- df[incd >= min.items,]
  #return(qq)
  return(df)
}

makeTrainSets <- function(df.c){
  df.tr <- df.c %>% 
    filter(row_number() < 0.8*nrow(df.c)) %>% 
    mutate(rs = rowSums(.)) %>% 
    filter(rs>1) %>% 
    select(-rs) 
  
  df.ts <- df.c %>% 
    filter(row_number() >= 0.8*nrow(df.c) & row_number() < 0.9*nrow(df.c)) %>% 
    mutate(rs = rowSums(.)) %>% 
    filter(rs>1) %>% 
    select(-rs) 
  
  df.va <- df.c %>% 
    filter(row_number() >= 0.9*nrow(df.c)) %>% 
    mutate(rs = rowSums(.)) %>% 
    filter(rs>1) %>% 
    select(-rs) 
  
  list(df.tr=df.tr, df.ts=df.ts, df.va=df.va)
  
}

makeRecommendlabTrainSets <- function(df.c){
  require(recommenderlab)
  df.c <- df.c %>% as(.,"binaryRatingMatrix")
  
  tr <- floor(nrow(df.c)*0.8)
  ts <- floor(nrow(df.c)*0.9)
  
  df.tr <- df.c[1:tr-1,]
  df.ts <- df.c[tr:ts-1,]
  df.va <- df.c[ts:nrow(df.c),]

  list(df.tr=df.tr, df.ts=df.ts)
}

batchGenerator <- function(size=1000, mockup=mockup, db="db.sqlite", test=0, use_date=F, merged=F, min_incid=2){
  function(){
    con <- dbConnect(RSQLite::SQLite(), db)
    
    #select n random users
    # 2x faster, but much less records?
    #u <- dbSendQuery(con, "select user from users where cnt > 1 and test = :test and random() % 2 = 0 limit :size")
    
    u <- dbSendQuery(con, "select user from users where cnt > 1 and test = :test order by random() limit :size")
    
    #rework following if ever needed
    dbBind(u, list(size = size, test = test))
    usrs <- dbFetch(u)
    dbClearResult(u)
    
    #select their items
    i <- dbSendQuery(con, "select id, user, prod, quan from orders where user in ($user) order by user")
    dbBind(i, list(user=usrs$user))
    df <- dbFetch(i)
    dbClearResult(i)
    
    df <- df[df$prod %in% names(mockup),]
    
    df %<>% 
      transformData()
    
    usrs <- rownames(df)
     
    if(length(mockup) > 0) {
      df <- df[,names(df) %in% names(mockup)]
      df <- bind_rows(df, mockup)
      rownames(df) <- usrs
    }
    

    df %<>%
      imputeNas() %>%
      as.matrix()
    
    incidence <- rowSums(df)

    if(use_date==T){
      
      q <- dbSendQuery(con, "select date from users where user in (?)")
      dbBind(q, list(usrs))
      dates <- dbFetch(q)$date
      dbClearResult(q)
      dates <- as.Date(dates, origin='1970-01-01')
      d_month <- month(dates)
      d_wday <- wday(dates)
      df_dates <- cbind(d_month,d_wday)
      
      if(merged == F){
        res <- list(
          list(df[incidence >= min_incid,], df_dates[incidence >= min_incid,]), 
          list(df[incidence >= min_incid,], df[incidence >= min_incid,])
          )
        
      } else {
        colnames(df_dates) <- paste0('aux_', colnames(df_dates))
        res <- list(cbind(df[incidence >= min_incid,], df_dates[incidence >= min_incid,]), 
                    df[incidence >= min_incid,])
      }
      
    } else {
      
      res <- list(df[incidence >= min_incid,], df[incidence >= min_incid,])
      
    }

    dbDisconnect(con)

    res

     }
}
