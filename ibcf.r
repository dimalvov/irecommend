#baseline? recommender with IBCF
library(recommenderlab)
con <- dbConnect(RSQLite::SQLite(), "db.sqlite")
#select 
#df <- dbGetQuery(con, "select id, user, prod, quan from orders limit 100000")

df <- batchGenerator(size=1000000, mockup=mockup)()[[1]]

#create train, test, validate sets
d <- makeRecommendlabTrainSets(df)

df.tr <- d$df.tr
df.ts <- d$df.ts
df.va <- d$df.va

#simple recommender
model <- Recommender(df.tr, method="IBCF")

pred <- predict(object=model, newdata=df.ts, n=3)
as(pred, "list")[1:10]


aa<-batchGenerator(size=10000, mockup = mockup, test=1)()

df.va <- a[[1]] %>% 
  as.data.frame() %>% 
  mutate(rn=rowSums(.)) %>% 
  filter(rn>1) %>% 
  select(-rn) %>% 
  as.matrix()

b <- sapply((1:nrow(df.va)),function(x) {assessGuess(df.va[x,,drop=F], model, 3, 'ibcf')})
summary(b)

netPredictOne(aa[[1]][sample(nrow(aa[[1]]),1),,drop=F], model = model, method='ibcf', n=3) %>% textPredict(dict=dict)

b %>% textPredict(dict=dict) %>% unlist() %>% table() %>% sort()

