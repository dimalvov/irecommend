library(keras)
library(magrittr)
library(lubridate)
library(tidyr)
library(tictoc)
library(stringi)
library(data.table)
library(dplyr)
library(DBI)

source('./rfuns.r')

dictData <- read.csv('./data/old/PRODUCT.csv')
dict <- as.character(dictData$P_NAME)
names(dict) <- as.numeric(dictData$P_CODE)

con <- dbConnect(RSQLite::SQLite(), "db.sqlite")
df <- dbGetQuery(con, "select id, user, prod, quan from orders limit 100000")

tic()
df.c <- df %>% transformData(., min.incidence=10)
toc()

#check
#sum(df$quan[df$user=='-2139963889'])
#rowSums(df.c[df.c$user=='-2139963889',-grep('user', colnames(df.c))])


#split
d <- makeTrainSets(df.c)
df.tr <- d$df.tr %>% as.matrix()
df.ts <- d$df.ts %>% as.matrix()
df.va <- d$df.va %>% as.matrix()


#summarize
df.tr %>% colSums() %>% sort(decreasing=T) %>% head()

#learn
net <- keras_model_sequential()
net %>%
  layer_dense(units = 100, activation = "relu", input_shape = ncol(df.tr)) %>%
  #layer_dense(units = 5000, activation = "relu", input_shape = ncol(df.tr)) %>%
  #layer_dense(units = 1000, activation = "relu") %>%
  #layer_dense(units = 5000, activation = "relu") %>%
  layer_dense(units = ncol(df.tr))

summary(net)

net %>% compile(
  loss = "mean_squared_error", 
  optimizer = "adam"
)

checkpoint <- callback_model_checkpoint(
  filepath = "model.hdf5", 
  save_best_only = TRUE, 
  period = 1,
  verbose = 1
)

early_stopping <- callback_early_stopping(patience = 3)

net %>% fit(
  x = df.tr, 
  y = df.tr, 
  epochs = 10, 
  batch_size = 32,
  validation_data = list(df.ts, df.ts),
  callbacks = list(checkpoint, early_stopping)
)

set.seed(42)
#predict one
df.va[sample(nrow(df.va),1),,drop=F] %>% netPredictOne(., model=net, n=3)  %>% textPredict(dict=dict)

#assess our guess
a <- sapply((1:nrow(df.va)),function(x) {assessGuess(df.va[x,,drop=F], net, n=3, cover=NULL)})
summary(a)

#assess random guess
b <- sapply((1:nrow(df.va)),function(x) {assessGuess(df.va[x,,drop=F], net, 3, 'r')})
summary(b)

#assess most frequent product guess
f <- colSums(df.va) %>% sort(decreasing = T) %>% head(3)
c <- sapply((1:nrow(df.va)),function(x) {assessGuess(df.va[x,,drop=F], net, 3, 'f', f=f)})
summary(c)

#assess our guess with coverage
cover <- colSums(df.ts) %>% sort(decreasing = T) %>% head(100) %>% names()
g <- sapply((1:nrow(df.ts)),function(x) {assessGuess(df.ts[x,,drop=F], net, 3, cover=cover)})
summary(g)

a[which(rowSums(df.ts)>1)] %>% mean(na.rm=T)
a[which(rowSums(df.ts)>2)] %>% mean(na.rm=T)
a[which(rowSums(df.ts)>3)] %>% mean(na.rm=T)
a[which(rowSums(df.ts)>4)] %>% mean(na.rm=T)

